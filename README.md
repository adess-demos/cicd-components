# Component - Rust Basic

> **Note**
>
> This is a demo and learning project for CI/CD components. Feel free to fork it into your own environments.
> It will be migrated into an official component eventually, follow updates in https://gitlab.com/groups/gitlab-org/-/epics/12290  

This component provides a basic template to build and test Rust projects, including best practices such as caching.

```
templates/
  rust-basic.yml
README.md 
```

It is the first CI/CD component project created by @dnsmichi -- you can inspect the Git history to learn. Treat this project with the required awareness -- it is far from perfect, and will be released in learning iterations.

This is a demo project, and not meant for production usage.

## Usage

**Note** This example is will be updated while more features are added in the learning process. 

```
stages: 
  - build
  - test

include:
  - component: gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/rust-basic@0.7
      inputs:
        stage_build: build
        stage_test: test 
        rust_version: latest

  - component: gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/rust-basic@0.7
      inputs:
        rust_version: 1.72.0        


  # Seperate jobs
  - component: gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/build@0.7
      inputs:
        stage: build
        rust_version: latest
  - component: gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/test@0.7
      inputs:
        stage: test
        rust_version: latest

```

You can also specify tagged releases instead of `main`, which defaults to the latest HEAD in the default branch.

- [0.1](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.1) is the MVC working version.
- [0.2](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.2) adds job templates.
- [0.3](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.3) adds support for Rust caching. 
- [0.4](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.4) adds support for inputs: `stage_build` and `stage_test`. 
- [0.5](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.5) adds support for inputs: `rust_version` to specify the image tag, and dynamically generate CI/CD names based on the version. 
- [0.6](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.6) adds release automation.
- [0.7](https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/components-templates/rust-basic/-/releases/0.7) refactors the `rust-basic` template into two separate job templates: `build` and `test. 

You include multiple components with different versions, and inputs. 

### Individual jobs

You can add the jobs in this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/rust/build@<VERSION>
      inputs:
        stage: build
        rust_version: latest
  - component: gitlab.com/components/rust/test@<VERSION>
      inputs:
        stage: test
        rust_version: latest
```

where `<VERSION>` is the latest released tag or `main`.

#### Inputs

##### Build

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `build` | The build stage name |
| `rust_version` | `latest` | The Rust image tag version from https://hub.docker.com/_/rust |

##### Test

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | The test stage name |
| `rust_version` | `latest` | The Rust image tag version from https://hub.docker.com/_/rust |

### Full pipeline

You can add the full pipeline component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/rust/rust-basic@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

#### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage_test` | `test` | The test stage name |
| `stage_build` | `build` | The build stage name |
| `rust_version` | `latest` | The Rust image tag version |


## Tests

The CI/CD component is tested in [.gitlab-ci.yml](.gitlab-ci.yml). The jobs run `cargo` which requires

1. `Cargo.toml` as project configuration
2. Sample Rust code in `src/main.rs`

## Resources

- Consumer demo project in https://gitlab.com/gitlab-de/use-cases/cicd-components-catalog/consumers/rust-hello-component 